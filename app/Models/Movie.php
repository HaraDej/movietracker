<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Auth;

class Movie extends Model
{
    use HasFactory;

    public $fillable = [
        'title',
        'imdb_id',
        'poster_url',
    ];

    public function viewers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, MovieView::class)->withPivot('rating');
    }

    public function movieViews(): HasMany
    {
        return $this->hasMany(MovieView::class);
    }

    public function avgRating(): string
    {
        return $this->movieViews->avg('rating') ?? 0;
    }
}
