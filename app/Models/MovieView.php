<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Relations\BelongsToOne;

class MovieView extends Pivot
{
    public $fillable = [
        'rating',
    ];

    public function viewer(): BelongsToOne
    {
        return $this->belongsToOne(User::class);
    }

    public function movie(): BelongsToOne
    {
        return $this->belongsToOne(Movie::class);
    }
}
