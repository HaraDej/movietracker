<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Collection;
use Illuminate\Http\Client\Response;

class OmdbClient
{
    public function search(string $query): ?Collection
    {
        $response = Http::get(config('omdb.base_url') . '&s=' . urlencode($query));
        $response->throwIf(static fn (Response $response) => $response->json('Response') !== 'True');
        return collect($response->json('Search'));
    }
}