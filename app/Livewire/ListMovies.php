<?php

namespace App\Livewire;

use Auth;
use App\Models\Movie;
use Livewire\Attributes\Computed;
use Livewire\Component;
use Livewire\WithPagination;

class ListMovies extends Component
{
    use WithPagination;
    
    protected $listeners = [
        'refreshParentComponent' => '$refresh',
    ];

    #[Computed]
    public function movies()
    {
        return Movie::withAvg('movieViews as avg_rating', 'rating')
        ->with('viewers')
        ->paginate(2);
    }
    
    public function render()
    {
        return view('livewire.list-movies');
    }
}
