<?php

namespace App\Livewire;

use App\Models\Movie as Model;
use Livewire\Component;
use Livewire\Attributes\Computed;

class Movie extends Component
{
    public $movie;

    #[Computed]
    public function rating()
    {
        return $this->movie->viewers->firstWhere('id', auth()->user()->id)->pivot->rating ?? 0;
    }

    public function rate($rating)
    {
        $this->movie->viewers()->updateExistingPivot(auth()->user()->id, ['rating' => $rating]);
    }

    public function toggleViewed()
    {
        $this->movie->viewers()->toggle([auth()->user()->id => ['rating' => 0]]);
    }

    public function render()
    {
        return view('livewire.movie');
    }
}
