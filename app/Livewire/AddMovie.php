<?php

namespace App\Livewire;

use App\Models\Movie;
use Livewire\Component;
use Illuminate\Support\Str;

class AddMovie extends Component
{
    public $title;
    public $poster_url;
    public $imdb_id;

    public function mount(array $movie)
    {
        $this->title = $movie['Title'];
        $this->poster_url = $movie['Poster'];
        if (! Str::startsWith($this->poster_url, 'http')) {
            $this->poster_url = '';
        }
        $this->imdb_id = $movie['imdbID'];
    }

    public function add()
    {
        $entry = [
            'title' => $this->title,
            'poster_url' => $this->poster_url,
            'imdb_id' => $this->imdb_id,
        ];

        Movie::upsert($entry, uniqueBy: ['title']);

        session()->flash('message', 'Film hinzugefügt.');
    }

    public function render()
    {
        return view('livewire.add-movie');
    }
}
