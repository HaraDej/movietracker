<?php

namespace App\Livewire;

use App\Models\Movie;
use App\Services\OmdbClient;
use Livewire\Component;
use Illuminate\Http\Client\RequestException;

class SearchMovie extends Component
{
    public string $searchTitle = '';

    public function render(OmdbClient $client)
    {
        $movies = collect();

        if (!empty($this->searchTitle)) {
            try {
                $movies = $client->search($this->searchTitle);
            } catch (RequestException) {
                session()->flash('message', 'OMDB-Aufruf fehlgeschlagen! Bitte Api Key prüfen.');
            }
        }

        return view('livewire.search-movie', ['result' => $movies]);
    }
}
