<?php

return [
    'base_url' => "http://www.omdbapi.com/?apikey=" . env('OMDB_API_KEY', ''),
];
