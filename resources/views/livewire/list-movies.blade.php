<div>
    @foreach ($this->movies as $movie)
    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-3 text-gray-900">
                    <livewire:movie :$movie key="movie-{{ $movie->id }}" />
                </div>
            </div>
        </div>
    </div>
    @endforeach
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        {{ $this->movies->links() }}
    </div>
</div>
