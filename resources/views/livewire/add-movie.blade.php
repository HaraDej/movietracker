<div>
    @if (session()->has('message'))
    <div class="p-4 mb-4 text-sm text-green-800 rounded-lg bg-green-50 dark:bg-gray-800 dark:text-green-400" role="alert">
        <span class="font-medium">{{ session('message') }}</span>
    </div>
    @endif

    <dl class="w-full text-gray-900 divide-y divide-gray-200 dark:text-white dark:divide-gray-700">
        <div class="flex flex-wrap">
            <dt class="mb-1 w-64 text-gray-500 md:text-lg dark:text-gray-400">Filmtitel</dt>
            <dd class="text-lg font-semibold">{{ $this->title }}</dd>
        </div>
    
        @if (!empty($this->poster_url))
        <div class="flex flex-wrap">
            <dt class="mb-1 w-64 text-gray-500 md:text-lg dark:text-gray-400">Filmposter</dt>
            <dd class="text-lg font-semibold"><img class="my-2" width="100px" src="{{$this->poster_url}}" /></dd>
        </div>
        @endif

        <div class="flex flex-wrap">
            <dt class="mb-1 w-64 text-gray-500 md:text-lg dark:text-gray-400">Filmliste</dt>
            <dd class="text-lg font-semibold"><button type="button" class="px-5 me-2 mb-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-100 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700" wire:click="add">Hinzufügen</button></dd>
        </div>
    </dl>
    
</div>
