<div>
    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <label for="searchTitle" class="mr-5">Suche nach Filmtitel:</label> 
                    <input id="searchTitle" class="w-1/2" type="text" wire:model.live="searchTitle">
                </div>
            </div>
        </div>
    </div>

    @if (session()->has('message'))
    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-4 text-sm text-green-800 rounded-lg bg-green-50 dark:bg-gray-800 dark:text-green-400" role="alert">
                    <span class="font-medium">{{ session('message') }}</span>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if (! empty($this->searchTitle) && $result->isEmpty())
    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-4 text-sm text-yellow-800 rounded-lg bg-yellow-50 dark:bg-gray-800 dark:text-yellow-400" role="alert">
                    <span class="font-medium">Keine Filme gefunden mit "{{ $this->searchTitle }}"</span>
                </div>
            </div>
        </div>
    </div>
    @endif

    @foreach ($result as $movie)
    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-3 text-gray-900">
                    <livewire:add-movie :$movie :wire:key="$movie['imdbID']" />
                </div>
            </div>
        </div>
    </div>
    @endforeach

</div>
