<div>
    <dl class="w-full text-gray-900 divide-y divide-gray-200 dark:text-white dark:divide-gray-700">
        <div class="flex flex-wrap">
            <dt class="mb-1 w-64 text-gray-500 md:text-lg dark:text-gray-400">Filmtitel</dt>
            <dd class="text-lg font-semibold">{{ $this->movie->title }}</dd>
        </div>
    
        @if (!empty($this->movie->poster_url))
        <div class="flex flex-wrap">
            <dt class="mb-1 w-64 text-gray-500 md:text-lg dark:text-gray-400">Filmposter</dt>
            <dd class="text-lg font-semibold"><img class="my-2" width="100px" src="{{$this->movie->poster_url}}" /></dd>
        </div>
        @endif

        <div class="flex flex-wrap">
            <dt class="mb-1 w-64 text-gray-500 md:text-lg dark:text-gray-400">Durchschnittswertung</dt>
            <dd class="text-lg font-semibold">{{ $this->movie->avgRating() }} &starf;</dd>
        </div>
    
        @if ($this->movie->viewers->contains(auth()->user()))
        <div class="flex flex-wrap">
            <dt class="mb-1 w-64 text-gray-500 md:text-lg dark:text-gray-400">Wertung</dt>
            <dd class="text-lg font-semibold">
                    @for ($rate = 1; $rate <= 5; $rate++)
                    <span style="cursor:pointer" wire:click="rate({{ $rate }})">@if ($rate <= $this->rating) &starf; @else &star; @endif</span>
                    @endfor
            </dd>
        </div>
        @endif
        
        <div class="flex flex-wrap">
            <dt class="mb-1 w-64 text-gray-500 md:text-lg dark:text-gray-400">Angesehen</dt>
            <dd class="text-lg font-semibold"><input type="checkbox" wire:click="toggleViewed()" @if ($this->movie->viewers->contains(auth()->user()->id)) checked="checked" @endif></dd>
        </div>
    </dl>
</div>
