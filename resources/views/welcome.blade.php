<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <body class="antialiased">
        <div class="relative sm:flex sm:justify-center sm:items-center min-h-screen bg-dots-darker bg-center bg-gray-100 dark:bg-dots-lighter dark:bg-gray-900 selection:bg-red-500 selection:text-white">
            @if (Route::has('login'))
                <livewire:welcome.navigation />
            @endif

                <div class="sm:px-6 lg:px-8">
                    <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div class="p-6">
                            <h1 class="mb-4 text-4xl font-extrabold leading-none tracking-tight text-gray-900 md:text-5xl lg:text-6xl dark:text-white">Movietracker</h1>
                            <h2 class="mb-4 text-4xl font-extrabold dark:text-white">Erste Schritte</h2>
                            <ul class="space-y-1 text-gray-500 list-disc list-inside dark:text-gray-400">
                                <li>Zuerst als neuer Benutzer unter <strong>Register</strong> registrieren.</li>
                                <li>Über <strong>Filme suchen</strong> können Filme zur Liste hinzugefügt werden.</li>
                                <li>Unter dem Menüpunkt <strong>Filme sehen</strong> können Filme als angesehen markiert werden.</Li>
                                <li>Angesehene Filme können mit 1 bis 5 Sternen bewertet werden - dies hat Einfluss auf die Durchschnittsbewertung.</li>
                            </ul>
                        </div>
                    </div>
                </div>

        </div>
    </body>
</html>
