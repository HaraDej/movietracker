# Movietracker

## Name
Movie-Tracker, a demo project for showing skills in TALL (Tailwind CSS, Alpine.JS, Livewire, Laravel)

## Description
This is a small demonstration of how to connect to the OmDB API, provide a movie search and movie rating website for registered and logged in users.

## Installation
I assume you know how to [install](https://laravel.com/docs/10.x/installation) a Laravel project.
You need to have docker and php installed, and then everything is ready to go. Just use Laravel [sail](https://laravel.com/docs/10.x/sail)
You also need an Omdb [apikey](https://www.omdbapi.com/apikey.aspx) and to put it in the .env-File.

## Usage
Use your favorite browser and move to [localhost](http://localhost). You can register and rate movies, and view the average ratings of all movies.

## Live Demo
A live demo of this project can be found [here](http://movietracker.harald-jedele.de/).
A predefined user is `demo@example.com` using the password `Test1234`.

## Project status
This project was motivated by a former employer where it is given as a task to applicants.
I developed it in my free time as a skill showroom and maybe will change it to use different frameworks as well (Symfony, maybe just Laravel and Blade, etc.)